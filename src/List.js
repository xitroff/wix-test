import React from 'react';
import AddItemButton from "./AddItemButton";
import AddItemInput from "./AddItemInput";
import Item from "./Item";
import PropTypes from "prop-types";
import Common from './Common';

class List extends Common {
  static defaultProps = {
    items: [],
  };

  static propTypes = {
    items: PropTypes.array,
    onAddItemToList: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      showInput: false,
    };
  }

  onAddClick = name => {
    this.props.onAddItemToList(name);
    this.toggleShowInput();
  };

  render() {
    const items = this.props.items;

    return (
      <ul style={{border: '1px solid red'}}>

        {!this.state.showInput && <AddItemButton onButtonClick={this.toggleShowInput} />}

        {this.state.showInput &&
        <AddItemInput
          onAddClick={this.onAddClick}
          onCancelClick={this.toggleShowInput}
        />}

        {items.map(item =>
          <li key={item.key}>
            <Item
              {...item}

            />
          </li>)}
      </ul>
    );
  }
}

export default List;
