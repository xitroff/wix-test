import React from 'react';
import List from './List';
import v4 from 'uuid/v4';
import Common from "./Common";

const items = [
  {
    key: v4(),
    name: 'foo',
    parent: null,
    childrenInside: [],
  },
  {
    key: v4(),
    name: 'bar',
    parent: null,
    childrenInside: [
      {
        key: v4(),
        name: 'baz',
        parent: null,
        childrenInside: [
          {
            key: v4(),
            name: 'hello',
            parent: null,
            childrenInside: [],
          },
          {
            key: v4(),
            name: 'world',
            parent: null,
            childrenInside: [],
          },
        ],
      },
    ],
  },
  {
    key: v4(),
    name: 'two',
    parent: null,
    childrenInside: [],
  },
];

class App extends Common {
  constructor(props) {
    super(props);

    this.state = {
      items,
    };
  }

  render() {
    return (
      <List items={this.state.items} onAddItemToList={this.onAddItemToList} />
    );
  }
}

export default App;
