import React, {Component} from 'react';
import PropTypes from "prop-types";

class AddItemInput extends Component {
  static defaultProps = {
    show: false,
  };

  static propTypes = {
    show: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    const { show } = props;

    this.state = {
      show,
      value: '',
    };
  }

  handleChange = e => this.setState({value: e.target.value});

  onAddButtonClick = () => {
    const { onAddClick } = this.props;
    onAddClick(this.state.value);
  };

  render() {
    const {onCancelClick} = this.props;

    return (
      <div>
        <input type="text" value={this.state.value} onChange={this.handleChange} />
        <button onClick={this.onAddButtonClick}>Add</button>
        <button onClick={onCancelClick}>Cancel</button>
      </div>
    );
  }
}

export default AddItemInput;
