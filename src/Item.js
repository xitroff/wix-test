import React from 'react';
import PropTypes from "prop-types";
import List from './List';
import AddItemButton from "./AddItemButton";
import AddItemInput from "./AddItemInput";
import Common from './Common';

class Item extends Common {
  static defaultProps = {
    childrenInside: [],
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    childrenInside: PropTypes.array,
  };

  constructor(props) {
    super(props);

    this.state = {
      items: props.childrenInside,
      showInput: false,
    };
  }

  onAddClick = name => {
    this.onAddItemToList(name, () => {
      this.toggleShowInput();
    });
  };

  render() {
    const {name} = this.props;

    return (
      <div style={{border: '1px solid black'}}>

        {!this.state.showInput &&
        <AddItemButton
          buttonText="Add Inside"
          onButtonClick={this.toggleShowInput}
        />}

        {this.state.showInput &&
        <AddItemInput
          onAddClick={this.onAddClick}
          onCancelClick={this.toggleShowInput}
        />}

        <span>
          {name}
        </span>

        {this.state.items.length !== 0 &&
        <List
          items={this.state.items}
          onAddItemToList={this.onAddItemToList}
        />}

      </div>
    );
  }
}

export default Item;
