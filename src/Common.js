import {Component} from 'react';
import v4 from "uuid/v4";

class Common extends Component {
  toggleShowInput = () => this.setState(prevState => ({
    showInput: !prevState.showInput,
  }));

  onAddItemToList = (name, callback = null) => {
    if (name === '') {
      // debugger;
      return;
    }

    const newItem = {
      key: v4(),
      name,
      parent: null,
    };

    this.setState(prevState => ({
      items: [...prevState.items, newItem],
    }), callback);
  };
}

export default Common;
