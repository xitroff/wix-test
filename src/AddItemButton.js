import React from 'react';
import PropTypes from "prop-types";

const AddItemButton = ({buttonText, onButtonClick}) => (
  <button
    onClick={onButtonClick}
  >
    {buttonText}
  </button>
);

AddItemButton.defaultProps = {
  buttonText: 'Add item',
};

AddItemButton.propTypes = {
  buttonText: PropTypes.string,
  onButtonClick: PropTypes.func.isRequired,
};

export default AddItemButton;
